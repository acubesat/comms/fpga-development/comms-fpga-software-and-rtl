#include "xparameters.h"
#include "xil_printf.h"
#include "xgpio.h"
#include "xil_types.h"
#include <array>

// Get device IDs from xparameters.h
#define BTN_ID XPAR_AXI_GPIO_BUTTONS_DEVICE_ID
#define LED_ID XPAR_AXI_GPIO_LED_DEVICE_ID
constexpr auto BTNChannel = 1;
constexpr auto LEDChannel = 1;
constexpr auto BTNMask = 0xf;
constexpr auto LEDMask = 0xf;

int main() {
	XGpio_Config *cfg_ptr;
	XGpio led_device, btn_device;
	u32 data;

	xil_printf("Entered function main\r\n");

	// Initialize LED Device
	cfg_ptr = XGpio_LookupConfig(LED_ID);
	XGpio_CfgInitialize(&led_device, cfg_ptr, cfg_ptr->BaseAddress);

	// Initialize Button Device
	cfg_ptr = XGpio_LookupConfig(BTN_ID);
	XGpio_CfgInitialize(&btn_device, cfg_ptr, cfg_ptr->BaseAddress);

	// Set Button Tristate
	XGpio_SetDataDirection(&btn_device, BTNChannel, BTNMask);

	// Set Led Tristate
	XGpio_SetDataDirection(&led_device, LEDChannel, 0);

	for(;;) {
		data = XGpio_DiscreteRead(&btn_device, BTNChannel);
		data &= BTNMask;
		if (data != 0) {
			xil_printf("have mask\r\n");
			data = LEDMask;
		} else {
			data = 0;
		}
		XGpio_DiscreteWrite(&led_device, LEDChannel, data);
	}

	return 0;
}
