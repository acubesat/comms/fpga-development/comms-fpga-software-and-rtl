# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\Users\nikos\SpaceDot\FPGADevel\MixedDevelopment\PModIPs_wrapper_2\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\Users\nikos\SpaceDot\FPGADevel\MixedDevelopment\PModIPs_wrapper_2\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {PModIPs_wrapper_2}\
-hw {C:\Users\nikos\SpaceDot\FPGADevel\MixedDevelopment\pmods\PModIPs_wrapper.xsa}\
-out {C:/Users/nikos/SpaceDot/FPGADevel/MixedDevelopment}

platform write
domain create -name {freertos10_xilinx_ps7_cortexa9_0} -display-name {freertos10_xilinx_ps7_cortexa9_0} -os {freertos10_xilinx} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {empty_application}
platform generate -domains 
platform active {PModIPs_wrapper_2}
domain active {zynq_fsbl}
domain active {freertos10_xilinx_ps7_cortexa9_0}
platform generate -quick
platform generate
