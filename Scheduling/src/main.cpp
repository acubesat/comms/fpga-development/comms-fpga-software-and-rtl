#include <xil_types.h>
#include <xgpio.h>
#include <xil_printf.h>
#include <FreeRTOS.h>
#include <task.h>
#include <sleep.h>

#define BTN_ID XPAR_AXI_GPIO_BUTTONS_DEVICE_ID
#define LED_ID XPAR_AXI_GPIO_LED_DEVICE_ID
constexpr auto BTNChannel = 1;
constexpr auto LEDChannel = 1;
constexpr auto LEDMask = 0b1;
constexpr auto BTNMask = 0b1;

void blinky1(void* params) {
	volatile u32 data;
	XGpio* ledDevice = static_cast<XGpio*>(params);
	xil_printf("blink1\r\n");

	for (;;) {
		xil_printf("in blink1\r\n");
		data = LEDMask;
		XGpio_DiscreteWrite(ledDevice, LEDChannel, data);
		usleep(500000);
		data = 0;
		XGpio_DiscreteWrite(ledDevice, LEDChannel, data);
		usleep(500000);
	}
}

void blinky2(void* params) {
	volatile u32 data;
	XGpio* ledDevice = static_cast<XGpio*>(params);
	xil_printf("blink2\r\n");

	for (;;) {
		xil_printf("in blink2\r\n");
		data= LEDMask;
		XGpio_DiscreteWrite(ledDevice, LEDChannel, data);
		usleep(200000);
		data = 0;
		XGpio_DiscreteWrite(ledDevice, LEDChannel, data);
		usleep(500000);
	}
}

int main() {
	XGpio_Config *cfg_ptr;
	XGpio ledDevice, btn_device;

	xil_printf("Entered function main\r\n");

	// Initialize LED Device
	cfg_ptr = XGpio_LookupConfig(LED_ID);
	XGpio_CfgInitialize(&ledDevice, cfg_ptr, cfg_ptr->BaseAddress);

	// Initialize Button Device
	cfg_ptr = XGpio_LookupConfig(BTN_ID);
	XGpio_CfgInitialize(&btn_device, cfg_ptr, cfg_ptr->BaseAddress);

	// Set Button Tristate
	XGpio_SetDataDirection(&btn_device, BTNChannel, BTNMask);

	// Set Led Tristate
	XGpio_SetDataDirection(&ledDevice, LEDChannel, 0);

	xTaskCreate(blinky1, "Blink 1", 2000, static_cast<void*>(&ledDevice), tskIDLE_PRIORITY + 1, nullptr);
	xTaskCreate(blinky2, "Blink 2", 2000, static_cast<void*>(&ledDevice), tskIDLE_PRIORITY + 1, nullptr);
	vTaskStartScheduler();
	for (;;);

	return 0;
}
