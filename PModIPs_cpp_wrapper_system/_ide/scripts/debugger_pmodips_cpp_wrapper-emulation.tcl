# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: C:\Users\nikos\SpaceDot\FPGADevel\MixedDevelopment\PModIPs_cpp_wrapper_system\_ide\scripts\debugger_pmodips_cpp_wrapper-emulation.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source C:\Users\nikos\SpaceDot\FPGADevel\MixedDevelopment\PModIPs_cpp_wrapper_system\_ide\scripts\debugger_pmodips_cpp_wrapper-emulation.tcl
# 
connect -url tcp:localhost:4355
targets 3
dow C:/Users/nikos/SpaceDot/FPGADevel/MixedDevelopment/PModIPs_cpp_wrapper/Debug/PModIPs_cpp_wrapper.elf
targets 3
con
