# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: C:\Users\nikos\SpaceDot\FPGADevel\MixedDevelopment\Scheduling_system\_ide\scripts\debugger_scheduling-emulation.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source C:\Users\nikos\SpaceDot\FPGADevel\MixedDevelopment\Scheduling_system\_ide\scripts\debugger_scheduling-emulation.tcl
# 
connect -url tcp:localhost:4354
targets 3
dow C:/Users/nikos/SpaceDot/FPGADevel/MixedDevelopment/Scheduling/Debug/Scheduling.elf
targets 3
con
